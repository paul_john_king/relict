package relict

import (
	"fmt"
	"io"
	"os"
	"os/exec"
)

/*
TODO: Document this type.

*/
type Image struct {
	Name string `xml:",chardata"`
	Push bool   `xml:"push,attr"`
}

/*
TODO: Document this method.

*/
func (image *Image) Build(build *Build, tag string) (writer io.WriteCloser, wait func(*error), kill func(), err error) {
	var (
		arguments []string
		command   *exec.Cmd
	)

	build.AppendConfigToArguments(&arguments)
	arguments = append(arguments, `build`, `-t`, tag, `-`)

	command = exec.Command(`docker`, arguments...)
	writer, err = command.StdinPipe()
	if err != nil {
		return
	}
	command.Stdout = os.Stderr
	command.Stderr = os.Stderr
	err = command.Start()
	if err != nil {
		return
	}
	wait = func(err *error) {
		var (
			_err error
		)

		_err = command.Wait()
		if *err == nil {
			*err = _err
		}
		return
	}
	kill = func() {
		command.Process.Kill()
		return
	}
	return
}

/*
TODO: Document this method.

*/
func (image *Image) Tag(build *Build, buildTag string) (err error) {
	var (
		tag       string
		arguments []string
		command   *exec.Cmd
	)

	tag = build.AppendVersion(build.PrependRegistry(image.Name))

	arguments = append(arguments, `tag`, buildTag, tag)

	command = exec.Command(`docker`, arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stderr
	command.Stderr = os.Stderr
	err = command.Run()
	if err != nil {
		return
	}
	if image.Push {
		var (
			arguments []string
			command   *exec.Cmd
		)

		build.AppendConfigToArguments(&arguments)
		arguments = append(arguments, `push`, tag)

		command = exec.Command(`docker`, arguments...)
		command.Stdin = os.Stdin
		command.Stdout = os.Stderr
		command.Stderr = os.Stderr
		err = command.Run()
		if err != nil {
			return
		}
	}
	return
}

/*
TODO: Document this type.

*/
type Images []*Image

/*
TODO: Encapsulate and document this method.  Say that it returns an error if
'images' is empty.

*/
func (images *Images) BuildAndTag(build *Build) (writer io.WriteCloser, wait func(*error), err error) {
	var (
		buildImage *Image
		buildTag   string
		buildWait  func(*error)
		buildKill  func()
	)

	if len(*images) == 0 {
		err = fmt.Errorf("there are no images to build")
		return
	}
	buildImage = (*images)[0]
	buildTag = build.AppendVersion(build.PrependRegistry(buildImage.Name))
	writer, buildWait, buildKill, err = buildImage.Build(build, buildTag)
	if err != nil {
		return
	}
	wait = func(err *error) {
		var (
			_err error
		)

		_err = writer.Close()
		if *err == nil {
			*err = _err
		}
		if *err == nil {
			buildWait(err)
			if *err != nil {
				return
			}
			for _, image := range *images {
				*err = image.Tag(build, buildTag)
				if *err != nil {
					return
				}
			}
		} else {
			buildKill()
		}
		return
	}
	return
}
