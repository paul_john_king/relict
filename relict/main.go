//usr/bin/env go run "${0}" "${@}"; exit $?
//go:generate go fmt gitlab.com/paul_john_king/relict/relict

/*
TODO: Document this command.

*/
package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/paul_john_king/relict"
)

type Path relict.Path

func (path *Path) Set(str string) (err error) {
	*path = Path(str)
	return
}

func (path *Path) String() (str string) {
	str = string(*path)
	return
}

type Paths relict.Paths

/*
'Set' implements the 'Set' method signature of the 'flag.Value' interface.

*/
func (paths *Paths) Set(str string) (err error) {
	*paths = append(*paths, relict.Path(str))
	return
}

/*
'String' implements the 'String' method signature of the 'flag.Value' interface.

*/
func (paths *Paths) String() (str string) {
	str = fmt.Sprintf("%s", *paths)
	return
}

type PushImages relict.Images

func (images *PushImages) Set(str string) (err error) {
	var (
		image relict.Image
	)

	image.Name = str
	image.Push = true
	*images = append(*images, &image)
	return
}

func (images *PushImages) String() (str string) {
	var (
		names []string
	)

	for index, image := range *images {
		names[index] = image.Name
	}
	str = fmt.Sprintf("%s", names)
	return
}

type TagImages relict.Images

func (images *TagImages) Set(str string) (err error) {
	var (
		image relict.Image
	)

	image.Name = str
	image.Push = false
	*images = append(*images, &image)
	return
}

func (images *TagImages) String() (str string) {
	var (
		names []string
	)

	for index, image := range *images {
		names[index] = image.Name
	}
	str = fmt.Sprintf("%s", names)
	return
}

//	TODO: There is a fundamental difference between the relationship between
//	the relict Parameter type and this commands Parameter type, and the
//	relationship between, say, the relict Path type and this commands Path
//	type.  In the latter case, this command is trying to 'import' the relict
//	type, but in the former case, this command has an entirely different type
//	and it is only transfered (i) one parameter at a time (ii) using the
//	components of the command type.  Though there is some initial elegance to
//	'importing' relict types, I suspect that the elegance is misleading.  I
//	'recreated' the relict types here in order to move the implementation of
//	the 'Value' interface from relict (where it really didn't belong) to here
//	(where is does). The 'import' is just historical baggage.  The types here
//	should be solely concerned with extracting command-line options and
//	arguments, the relict types should be solely concerned with parsing XML,
//	adding extra build information and running the build.  I'm not entirely
//	sure where the handoff between the two type regimes should be, but it
//	should definitely be a lot clearer that is *is* in fact a handoff.

type Parameter struct {
	Name  string
	Value string
}

func (parameter *Parameter) Set(str string) (err error) {
	var (
		components []string
	)

	components = strings.SplitN(str, `:`, 2)
	if len(components) != 2 || len(components[0]) == 0 {
		err = fmt.Errorf("value must have form '«name»:«value»'")
		return
	}
	parameter.Name = components[0]
	parameter.Value = components[1]
	return
}

func (parameter *Parameter) String() (str string) {
	str = fmt.Sprintf("{Name: '%s'; Value: '%s'}", parameter.Name, parameter.Value)
	return
}

type Parameters []Parameter

func (parameters *Parameters) Set(str string) (err error) {
	var (
		parameter Parameter
	)

	err = parameter.Set(str)
	if err != nil {
		return
	}
	*parameters = append(*parameters, parameter)
	return
}

func (parameters *Parameters) String() (str string) {
	var (
		buffer bytes.Buffer
	)

	buffer.WriteString(`[`)
	for _, parameter := range *parameters {
		buffer.WriteString(parameter.String())
	}
	buffer.WriteString(`]`)
	str = buffer.String()
	return
}

func main() {
	var (
		tars       Paths
		dirs       Paths
		stdin      bool
		tagImages  TagImages
		pushImages PushImages
		contexts   Paths
		stdout     bool
		registry   string
		config     Path
		parameters Parameters
		buildFile  relict.Path
		build      relict.Build
	)

	flag.Usage = printUsage
	flag.Var(&tars, `tar`, "Add the content of the tar at the path `«path»` to the Docker context.")
	flag.Var(&dirs, `dir`, "Add the content of the directory at the path `«path»` to the Docker context.")
	flag.BoolVar(&stdin, `stdin`, false, "Add the content of a tar read from the standard input to the Docker context.")
	flag.Var(&tagImages, `tag`, "Build and tag a Docker image with the name `«name»`.")
	flag.Var(&pushImages, `push`, "Build, tag and push a Docker image with the name `«name»`.")
	flag.Var(&contexts, `context`, "Write a Docker context to the path `«path»`.")
	flag.BoolVar(&stdout, `stdout`, false, "Write a Docker context to the standard output.")
	flag.StringVar(&registry, `registry`, ``, "Given `«host»[:«port»][/«author»]`, prefix all image tags with registry host «host», optional registry port «port» and optional image author «author».")
	flag.Var(&config, `config`, "Read the Docker client configuration from the path `«path»`.")
	flag.Var(&parameters, `parameter`, "Given `«name»:«value»`, set the parameter with the name «name» to the value «value».")
	flag.Parse()
	switch flag.NArg() {
	case 0:
		buildFile = relict.Path(`relict.xml`)
	case 1:
		buildFile = relict.Path(flag.Arg(0))
	default:
		fmt.Fprintf(os.Stderr, "too many command-line arguments\n")
		flag.Usage()
		os.Exit(2)
	}

	checkError(build.Init(buildFile), 1)
	checkError(build.AddStdinToSources(stdin), 1)
	checkError(build.AddTarsToSources(tars), 1)
	checkError(build.AddDirsToSources(dirs), 1)
	checkError(build.AddStdoutToTargets(stdout), 1)
	checkError(build.AddImagesToTargets(relict.Images(tagImages)), 1)
	checkError(build.AddImagesToTargets(relict.Images(pushImages)), 1)
	checkError(build.AddContextsToTargets(contexts), 1)
	checkError(build.SetRegistry(registry), 1)
	checkError(build.SetConfig(relict.Path(config)), 1)
	for _, parameter := range parameters {
		checkError(build.SetParameter(parameter.Name, parameter.Value), 1)
	}
	checkError(build.Run(), 1)
}

func checkError(err error, status int) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(status)
	}
	return
}

func printUsage() {
	fmt.Fprintf(os.Stderr, `Usage:

    relict [options...] [«path»]

Following the specifications of the build file at path «path» (or 'relict.xml'
if «path» is not specified) and the command-line options, generate a Docker
context and from the context write Docker contexts and build, tag and
optionally push Docker images.

Options:

`)
	flag.PrintDefaults()
}
