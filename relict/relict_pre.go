//usr/bin/env go run "${0}" "${@}"; exit $?
//+build ignore

package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	checkError(os.RemoveAll(`content`), 1)
	checkError(os.MkdirAll(`content/bin`, 0755), 1)
	checkError(os.Link(`docker`, `content/bin/docker`), 1)
	for _, components := range [][]string{
		{`go`, `fmt`, `main.go`},
		{`go`, `vet`, `main.go`},
		{`GOARCH=amd64`, `GOOS=linux`, `go`, `build`, `-o`, `content/bin/relict`, `main.go`},
	} {
		checkError(callCommand(components), 1)
	}
}

func checkError(err error, status int) {
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(status)
	}
	return
}

func callCommand(components []string) (err error) {
	var (
		environment []string
		name        string
		arguments   []string
		command     *exec.Cmd
	)

	fmt.Printf("calling %q\n", strings.Join(components, ` `))
	for index, component := range components {
		if strings.Contains(component, `=`) {
			environment = append(environment, component)
		} else {
			name = component
			arguments = components[index+1:]
			break
		}
	}
	command = exec.Command(name, arguments...)
	command.Env = append(os.Environ(), environment...)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	err = command.Run()
	if err != nil {
		return
	}
	return
}
