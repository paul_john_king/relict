package relict

import (
	"compress/gzip"
	"io"
)

/*
A 'GzipWriter' instance is a writer that writes gzip-compressed data to its
underlying writers.

*/
type GzipWriter struct {
	*gzip.Writer
}

/*
'Open' tries to open and initialise 'gzipWriter' with underlying writers
'writers', and returns a function 'close' that tries to close and uninitialise
'gzipWriter' (but does *not* close its underlying writers).  'Open' fails if
'gzipWriter' is already initialised.  'close' accepts a pointer to an error.
If the error is 'nil' then 'close' updates it with its own error status,
otherwise 'close' leaves the error unchanged.

*/
func (gzipWriter *GzipWriter) Open(writers ...io.Writer) (close func(*error), err error) {
	err = Initialised(gzipWriter, false)
	if err != nil {
		return
	}
	gzipWriter.Writer = gzip.NewWriter(io.MultiWriter(writers...))
	close = func(err *error) {
		var (
			_err error
		)

		_err = gzipWriter.Writer.Close()
		if *err == nil {
			*err = _err
		}
		gzipWriter.Writer = nil
		return
	}
	return
}

/*
'Initialised' returns 'true' iff 'gzipWriter' has been initialised by calling
its 'Open' method.

*/
func (gzipWriter *GzipWriter) Initialised() bool {
	return gzipWriter.Writer != nil
}
