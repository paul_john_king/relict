package relict

type Parameter struct {
	Name  string `xml:"parameter,attr"`
	Query string `xml:",chardata"`
	Value string
	Set   bool
}
