package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main() {
	var (
		err     error
		image string
		pwd     string
		command *exec.Cmd
	)

	if len(os.Args) == 2 {
		image = os.Args[1]
	} else {
		err = fmt.Errorf(`Usage: %s «image»`, os.Args[0])
	}
	checkError(err, 2)

	pwd, err = os.Getwd()
	checkError(err, 1)

	command = exec.Command(
		`docker`,
		`run`,
		`--rm`,
		`-a`, `stdin`,
		`-a`, `stdout`,
		`-a`, `stderr`,
		`--interactive`,
		`--tty`,
		`--volume`, `/var/run/docker.sock:/var/run/docker.sock`,
		`--volume`, fmt.Sprintf(`%s:/relict`, pwd),
		image,
	)
	command.Env = os.Environ()
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	checkError(command.Run(), 1)

	os.Exit(0)
}

func checkError(err error, status int) {
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(status)
	}
	return
}
