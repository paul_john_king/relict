package relict

import (
	"fmt"
	"io"
	"os"
	"os/exec"
)

/*
A 'Run' instance is a system execution of 'docker run --rm --interactive
«image» «arg₁» ... «argₙ»', where the 'Tag' holds '«image»' and the 'Args'
field holds slice '«arg₁»', ..., '«argₙ«'.

TODO: Redocument this type.  It might well be that this type is a 'Stage', a
'Step' or a 'Process' in a pipeline, and the 'Run.Start' method should really
be the 'Stage.Run', 'Step.Run' or 'Process.Run' method.

TODO: Try and parse the <use> elements directly into a map from parameter names
to variable names.

*/
type Run struct {
	Tag  string `xml:"tag,attr"`
	Uses []struct {
		Parameter string `xml:"parameter,attr"`
		Variable  string `xml:"as,attr"`
	} `xml:"use"`
	Env  []string
	Args []struct {
		Arg string `xml:",innerxml"`
	} `xml:"arg"`
}

/*
TODO: Document this method.

*/
func (run *Run) Start(build *Build, inputReader io.Reader) (outputReader io.Reader, wait func(*error), err error) {
	var (
		arguments []string
		command   *exec.Cmd
	)

	build.AppendConfigToArguments(&arguments)
	arguments = append(arguments, `run`, `--rm`, `--interactive`)
	for _, use := range run.Uses {
		arguments = append(arguments, fmt.Sprintf("--env=%s", use.Variable))
	}
	arguments = append(arguments, build.PrependRegistry(run.Tag))
	for _, arg := range run.Args {
		arguments = append(arguments, arg.Arg)
	}

	command = exec.Command(`docker`, arguments...)
	command.Env = append(os.Environ(), run.Env...)
	command.Stdin = inputReader
	outputReader, err = command.StdoutPipe()
	if err != nil {
		return
	}
	command.Stderr = os.Stderr
	err = command.Start()
	if err != nil {
		return
	}
	wait = func(err *error) {
		if *err == nil {
			*err = command.Wait()
		} else {
			command.Process.Kill()
		}
		return
	}
	return
}
