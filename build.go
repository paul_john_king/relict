//go:generate go fmt gitlab.com/paul_john_king/relict

package relict

import (
	"archive/tar"
	"bufio"
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

/*
A 'Build' instance represents a Dockerfile, a collection of sources from which
the contents of a Docker context should be read, and a collection of targets to
which Docker artifacts generated from the Dockerfile and the Docker context
should be written.  The 'Dockerfile' field holds the argument of the
Dockerfile's FROM instruction in its 'From' field, and the remainder of the
Dockerfile in its 'Body' field.  This separation simplifies reading and writing
the version of the Docker base image.  The 'Sources' field holds a flag in its
'Stdin' field that is true iff the members of a tar read from standard input
should be added to the Docker context; a list in its 'Tars' field of paths to
tar files whose members should then be added, in list order, to the Docker
context; and a list in its 'Dirs' field of paths to file-system directories
whose contents should then be added, in list order, to the Docker context.  The
'Targets' field holds a flag in its 'Stdout' field that is true iff a
gzip-compressed tar of the Dockerfile and the Docker context should be written
to standard output; a list in its 'Contexts' fields of paths to which
gzip-compressed tars of the Dockerfile and the Docker context should be
written; and a list in its 'Images' field of the Docker images that should be
built, tagged and possibly pushed from the Dockerfile and the Docker context.

TODO: Document 'Parameters' field.

TODO: Document 'Pipeline' field.

*/
type Build struct {
	Parameters []*Parameter `xml:"parameters>set"`
	Sources    struct {
		Tars  Paths `xml:"tar"`
		Dirs  Paths `xml:"dir"`
		Stdin bool  `xml:"stdin,attr"`
	} `xml:"sources"`
	Pipeline struct {
		Runs []*Run `xml:"run"`
	} `xml:"pipeline"`
	Dockerfile struct {
		From         string `xml:"from,attr"`
		WithRegistry bool   `xml:"with-registry,attr"`
		Body         string `xml:",chardata"`
	} `xml:"dockerfile"`
	Targets struct {
		Images   Images `xml:"image"`
		Contexts Paths  `xml:"context"`
		Stdout   bool   `xml:"stdout,attr"`
	} `xml:"targets"`
	registry   string
	config     Path
	parameters map[string]*Parameter
	version    string
	commit     string
	init       bool
}

/*
'Init' tries to initialise 'build' by populating it with data extracted from
the XML in a build file at path 'path'.  'Init' returns an error if 'build' has
already been initialised.

*/
func (build *Build) Init(path Path) (err error) {
	var (
		dir     Path
		buffer  []byte
		index   int
		subPath Path
	)

	err = Initialised(build, false)
	if err != nil {
		return
	}
	dir = path.Dir()
	dir, err = dir.Abs()
	if err != nil {
		return
	}
	buffer, err = path.ReadFile()
	if err != nil {
		return
	}
	err = xml.Unmarshal(buffer, &build)
	if err != nil {
		return
	}
	if len(build.Dockerfile.From) == 0 {
		err = fmt.Errorf("attribute 'from' of element '<build><dockerfile>' in build file '%s' is missing or empty", path)
		return
	}
	build.Dockerfile.Body = strings.TrimSpace(build.Dockerfile.Body)
	if len(build.Dockerfile.Body) > 0 {
		build.Dockerfile.Body += "\n"
	}
	for index, subPath = range build.Sources.Tars {
		if subPath.IsAbs() {
			build.Sources.Tars[index].Prepend(dir)
		}
	}
	for index, subPath = range build.Sources.Dirs {
		if subPath.IsAbs() {
			build.Sources.Dirs[index].Prepend(dir)
		}
	}
	for index, subPath = range build.Targets.Contexts {
		if subPath.IsAbs() {
			build.Targets.Contexts[index].Prepend(dir)
		}
	}
	build.version, build.commit, err = path.VersionAndCommit()
	if err != nil {
		return
	}
	build.parameters = make(map[string]*Parameter)
	for _, parameter := range build.Parameters {
		build.parameters[parameter.Name] = parameter
	}
	build.init = true
	return
}

/*
'Initialised' returns 'true' iff 'build' has been initialised by calling its
'Init' method.

*/
func (build *Build) Initialised() (init bool) {
	init = build.init
	return
}

/*
'AddTarsToSources' tries to append each path in 'paths' to
'build.Sources.Tars'.  'AddTarsToSources' returns an error if 'build' has not
previously been initialised by calling its 'Init' method.

*/
func (build *Build) AddTarsToSources(paths []Path) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.Sources.Tars = append(build.Sources.Tars, paths...)
	return
}

/*
'AddDirsToSources' tries to append each path in 'paths' to
'build.Sources.Dirs'.  'AddDirsToSources' returns an error if 'build' has not
previously been initialised by calling its 'Init' method.

*/
func (build *Build) AddDirsToSources(paths []Path) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.Sources.Dirs = append(build.Sources.Dirs, paths...)
	return
}

/*
'AddStdinToSources' tries to set 'build.Sources.Stdin' to 'true' if 'add' is
'true', but leave 'build.Sources.Stdin' unchanged otherwise.
'AddStdinToSources' returns an error if 'build' has not previously been
initialised by calling its 'Init' method.

*/
func (build *Build) AddStdinToSources(add bool) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	if add {
		build.Sources.Stdin = true
	}
	return
}

/*
'AddImagesToTargets' tries to append each image in 'images' to
'build.Targets.Images'.  'AddImagesToSources' returns an error if 'build' has
not previously been initialised by calling its 'Init' method.

*/
func (build *Build) AddImagesToTargets(images Images) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.Targets.Images = append(build.Targets.Images, images...)
	return
}

/*
'AddContextsToTargets' tries to append each path in 'paths' to
'build.Targets.Contexts'.  'AddImagesToSources' returns an error if 'build' has
not previously been initialised by calling its 'Init' method.

*/
func (build *Build) AddContextsToTargets(paths []Path) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.Targets.Contexts = append(build.Targets.Contexts, paths...)
	return
}

/*
'AddStdoutToTargets' tries to set 'build.Targets.Stdout' to 'true' if 'add' is
'true', but leave 'build.Targets.Stdout' unchanged otherwise.
'AddStdoutToTargets' returns an error if 'build' has not previously been
initialised by calling its 'Init' method.

*/
func (build *Build) AddStdoutToTargets(add bool) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	if add {
		build.Targets.Stdout = true
	}
	return
}

/*
TODO: Document this method.

*/
func (build *Build) SetRegistry(registry string) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.registry = registry
	return
}

/*
TODO: Document this method.

*/
func (build *Build) SetConfig(path Path) (err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	build.config = path
	return
}

/*
TODO: Document this method.

*/
func (build *Build) SetParameter(name string, value string) (err error) {
	var (
		recognised bool
	)

	_, recognised = build.parameters[name]
	if !recognised {
		err = fmt.Errorf("%q is not a recognised parameter name", name)
		return
	}
	build.parameters[name].Value = value
	build.parameters[name].Set = true
	return
}

/*
TODO: Document this method.

*/
func (build *Build) AppendConfigToArguments(arguments *[]string) {
	if !build.config.IsEmpty() {
		*arguments = append(*arguments, `--config`, string(build.config))
	}
	return
}

/*
TODO: Document this method.

*/
func (build *Build) PrependRegistry(in string) (out string) {
	if len(build.registry) == 0 {
		out = in
	} else {
		out = build.registry + `/` + in
	}
	return
}

/*
TODO: Document this method.

*/
func (build *Build) AppendVersion(in string) (out string) {
	var (
		versionRegexp *regexp.Regexp
		versionMatch  []string
	)

	versionRegexp = regexp.MustCompile(`:([^/]*)$`)
	versionMatch = versionRegexp.FindStringSubmatch(in)
	if len(versionMatch) == 2 {
		out = in + `_` + build.version
	} else {
		out = in + `:` + build.version
	}
	return
}

/*
'dockerfile' returns the content 'content' of a Dockerfile generated from
'build'.  'dockerfile' returns an error if 'build' has not previously been
initialised by calling its 'Init' method.

*/
func (build *Build) dockerfile() (content []byte, err error) {
	err = Initialised(build, true)
	if err != nil {
		return
	}
	var (
		buffer bytes.Buffer
	)

	buffer.WriteString(`FROM `)
	if build.Dockerfile.WithRegistry && len(build.registry) != 0 {
		buffer.WriteString(build.registry)
		buffer.WriteString(`/`)
	}
	buffer.WriteString(build.Dockerfile.From)
	buffer.WriteString("\n")
	if len(build.commit) > 0 {
		buffer.WriteString(`LABEL "commit"="`)
		buffer.WriteString(build.commit)
		buffer.WriteString(`"`)
		buffer.WriteString("\n")
	}
	buffer.WriteString(`COPY ["/", "/"]`)
	buffer.WriteString("\n")
	buffer.WriteString(build.Dockerfile.Body)
	content = buffer.Bytes()
	return
}

/*
TODO: Document this method.

*/
func (build *Build) Run() (err error) {
	var (
		stdioScanner          *bufio.Scanner
		targetWriters         []io.Writer
		targetGzipWriter      GzipWriter
		targetGzipWriterClose func(*error)
		targetTarWriter       TarWriter
		targetTarWriterClose  func(*error)
		dockerfile            []byte
		pipelineReader        io.Reader
		pipelineWriter        io.WriteCloser
		pipelineChannel       chan error
		sourceTarWriter       TarWriter
		sourceTarWriterClose  func(*error)
	)

	err = Initialised(build, true)
	if err != nil {
		return
	}

	//	Parameters
	//	----------

	//	TODO: Explain this.

	stdioScanner = bufio.NewScanner(os.Stdin)
	for _, parameter := range build.Parameters {
		if !parameter.Set {
			fmt.Fprintf(os.Stderr, "%s\n", parameter.Query)
			stdioScanner.Scan()
			err = stdioScanner.Err()
			if err != nil {
				return
			}
			parameter.Value = stdioScanner.Text()
			parameter.Set = true
		}
	}

	for _, run := range build.Pipeline.Runs {
		for _, use := range run.Uses {
			_, recognised := build.parameters[use.Parameter]
			if !recognised {
				err = fmt.Errorf("%q is not a recognised parameter name", use.Parameter)
				return
			}
			run.Env = append(run.Env, fmt.Sprintf("%s=%s", use.Variable, build.parameters[use.Parameter].Value))
		}
	}

	//	Targets
	//	-------

	//	I sequentially add a writer to each of the build's targets to a slice
	//	of target writers, and defer waiting or closing each target writer that
	//	must be started or opened in order to read what I write to it.

	if len(build.Targets.Images) > 0 {
		var (
			writer io.WriteCloser
			wait   func(*error)
		)

		writer, wait, err = build.Targets.Images.BuildAndTag(build)
		if err != nil {
			return
		}
		defer wait(&err)
		targetWriters = append(targetWriters, writer)
	}
	for _, path := range build.Targets.Contexts {
		var (
			writer io.WriteCloser
			close  func(*error)
		)

		writer, close, err = path.Create()
		if err != nil {
			return
		}
		defer close(&err)
		targetWriters = append(targetWriters, writer)
	}
	if build.Targets.Stdout {
		targetWriters = append(targetWriters, os.Stdout)
	}

	//	I wrap all of the target writers in a single target gzip writer, wrap
	//	the target gzip writer in turn in a target tar writer, and defer
	//	closing both writers -- the individual target writers that require
	//	closure are closed above in their own deferals.  I thus create
	//	something akin to the Unix pipeline
	//
	//		targetTarWriter | targetGzipWriter >targets
	//
	//	and defer waiting for first the target tar writer to close, then the
	//	target gzip writer to close, and finally the open targets to close.

	targetGzipWriterClose, err = targetGzipWriter.Open(targetWriters...)
	if err != nil {
		return
	}
	defer targetGzipWriterClose(&err)
	targetTarWriterClose, err = targetTarWriter.Open(targetGzipWriter)
	if err != nil {
		return
	}
	defer targetTarWriterClose(&err)

	//	Dockerfile
	//	----------

	//	I construct a Dockerfile and defer adding it to the target tar writer.
	//	I defer the addition to ensure that this Dockerfile is the last
	//	addition to the target tar writer, and so overwrites any Dockerfile
	//	that may already have been added to the target tar writer.

	dockerfile, err = build.dockerfile()
	if err != nil {
		return
	}
	defer func(writer TarWriter, dockerfile []byte, err *error) {
		if *err == nil {
			*err = writer.WriteFile(`Dockerfile`, 0644, 0, 0, dockerfile)
		}
		return
	}(targetTarWriter, dockerfile, &err)

	//	Pipeline
	//	--------

	//	I open a pipeline writer and a pipeline reader to respectively write
	//	data to and read data from a pipeline of commands.  I thus create
	//	something akin to the vacuous Unix pipeline
	//
	//		pipelineWriter | pipelineReader

	pipelineReader, pipelineWriter = io.Pipe()

	//	For each run in the build's pipeline, I
	//
	//	- start a concurrent 'docker run ‥' command that reads its standard
	//	input from the current pipeline reader,
	//
	//	- set the pipeline reader to now read from the standard output of the
	//	command, and
	//
	//	- defer waiting for the command to terminate.
	//
	//	Thus, the first command reads its standard input from the pipeline
	//	writer, each subsequent command reads its standard input from the
	//	standard output of its predecessor, and the pipeline reader reads from
	//	the standard output of the last command.  I thus create something akin
	//	to the Unix pipeline
	//
	//		pipelineWriter | command₁ | command₂ | ‥ | commandₙ |
	//		pipelineReader
	//
	//	and defer waiting for the commands to terminate in reverse order.

	for _, run := range build.Pipeline.Runs {
		var (
			wait func(*error)
		)

		pipelineReader, wait, err = run.Start(build, pipelineReader)
		if err != nil {
			return
		}
		defer wait(&err)
	}

	//	I start a concurrent routine that adds the content read from the
	//	pipeline reader to the target tar writer, and signals completion of the
	//	addition by writing the error returned by the addition to a pipeline
	//	channel.  I defer reading the error from the channel.  I thus create
	//	something akin to the Unix pipeline
	//
	//		pipelineWriter | command₁ | command₂ | ‥ | commandₙ |
	//		targetTarWriter | targetGzipWriter >targets
	//
	//	and defer waiting for first the commands and the routine to terminate
	//	(in reverse order), then the target tar and gzip writers to close, and
	//	finally the opened targets to close.

	pipelineChannel = make(chan error)
	go func(reader *io.Reader, channel chan error) {
		channel <- targetTarWriter.AddTar(tar.NewReader(*reader))
		return
	}(&pipelineReader, pipelineChannel)
	defer func(err *error, channel chan error) {
		if *err == nil {
			*err = <-channel
		}
		return
	}(&err, pipelineChannel)

	//	I defer waiting for the pipeline writer to close.  Note, during
	//	deferal, I must close the pipeline writer before I wait for the
	//	concurrent run commands or the concurrent tar addition routine to
	//	terminate, otherwise the commands and the routine block, waiting for
	//	non-existent input, and I deadlock.  Once I close the pipeline writer,
	//	however, each command in turn reads EOF from its standard input, writes
	//	EOF to its standard output, and terminates, thus unblocking its
	//	deferred wait.  And once the last command writes EOF to its standard
	//	output, the tar routine reads EOF from the pipeline reader, completes
	//	the tar addition, and writes to the pipeline channel, thus unblocking
	//	the deferred read from the channel.

	defer func(err *error) {
		var (
			_err error
		)

		_err = pipelineWriter.Close()
		if *err == nil {
			*err = _err
		}
		return
	}(&err)

	//	Sources
	//	-------

	//	I open a source tar writer that writes tar data to the pipeline writer,
	//	and defer closing the writer.  I thus create something akin to the Unix
	//	pipeline
	//
	//		sourceTarWriter | command₁ | command₂ | ‥ | commandₙ |
	//		targetTarWriter | targetGzipWriter >targets
	//
	//	and defer waiting for first the source tar writer to close, then the
	//	commands to terminate, then the target tar and gzip writers to close,
	//	and finally the open targets to close.

	sourceTarWriterClose, err = sourceTarWriter.Open(pipelineWriter)
	if err != nil {
		return
	}
	defer sourceTarWriterClose(&err)

	//	I sequentially add the contents of each source in the build's sources
	//	to the source tar writer, and defer closing those sources that must be
	//	opened.  I thus create something akin to the Unix pipeline
	//
	//		sourceTarWriter <sources | command₁ | command₂ | ‥ | commandₙ |
	//		targetTarWriter | targetGzipWriter >targets
	//
	//	and defer waiting for first the opened sources to close, then the
	//	source tar writer to close, then the commands to terminate, then the
	//	target tar and gzip writers to close, and finally the opened targets to
	//	close.

	for _, path := range build.Sources.Tars {
		var (
			fileReader      *os.File
			fileReaderClose func(*error)
		)

		fileReader, fileReaderClose, err = path.Open()
		if err != nil {
			return
		}
		defer fileReaderClose(&err)
		err = sourceTarWriter.AddTar(tar.NewReader(fileReader))
		if err != nil {
			return
		}
	}
	for _, path := range build.Sources.Dirs {
		err = sourceTarWriter.AddDir(path)
		if err != nil {
			return
		}
	}
	if build.Sources.Stdin {
		err = sourceTarWriter.AddTar(tar.NewReader(os.Stdin))
		if err != nil {
			return
		}
	}
	return
}

/*
'print' prints a humanly readable representation of 'build' to the operating
system's standard error.

*/
func (build *Build) print() {
	fmt.Fprintf(os.Stderr, "Parameters:\n")
	for _, parameter := range build.Parameters {
		fmt.Fprintf(os.Stderr, "  Name: %q\n", parameter.Name)
		fmt.Fprintf(os.Stderr, "  Query: %q\n", parameter.Query)
		fmt.Fprintf(os.Stderr, "  Value: %q\n", parameter.Value)
		fmt.Fprintf(os.Stderr, "  Set: %t\n", parameter.Set)
	}
	fmt.Fprintf(os.Stderr, "Sources:\n")
	fmt.Fprintf(os.Stderr, "  Tars:\n")
	for _, path := range build.Sources.Tars {
		fmt.Fprintf(os.Stderr, "    %q\n", path)
	}
	fmt.Fprintf(os.Stderr, "  Dirs:\n")
	for _, path := range build.Sources.Dirs {
		fmt.Fprintf(os.Stderr, "    %q\n", path)
	}
	fmt.Fprintf(os.Stderr, "  Stdin: %t\n", build.Sources.Stdin)
	fmt.Fprintf(os.Stderr, "Pipeline:\n")
	fmt.Fprintf(os.Stderr, "  Runs:\n")
	for _, run := range build.Pipeline.Runs {
		fmt.Fprintf(os.Stderr, "    Tag: %q\n", run.Tag)
		fmt.Fprintf(os.Stderr, "    Uses:\n")
		for _, use := range run.Uses {
			fmt.Fprintf(os.Stderr, "      Parameter: %q\n", use.Parameter)
			fmt.Fprintf(os.Stderr, "      Variable: %q\n", use.Variable)
		}
		fmt.Fprintf(os.Stderr, "    Env:\n")
		for _, setting := range run.Env {
			fmt.Fprintf(os.Stderr, "      %q\n", setting)
		}
		fmt.Fprintf(os.Stderr, "    Args:\n")
		for _, arg := range run.Args {
			fmt.Fprintf(os.Stderr, "      %q\n", arg.Arg)
		}
	}
	fmt.Fprintf(os.Stderr, "Dockerfile:\n")
	fmt.Fprintf(os.Stderr, "  From: %q\n", build.Dockerfile.From)
	fmt.Fprintf(os.Stderr, "  WithRegistry: %t\n", build.Dockerfile.WithRegistry)
	fmt.Fprintf(os.Stderr, "  Body:\n")
	fmt.Fprintf(os.Stderr, "----\n")
	fmt.Fprintf(os.Stderr, "%s", build.Dockerfile.Body)
	fmt.Fprintf(os.Stderr, "----\n")
	fmt.Fprintf(os.Stderr, "Targets:\n")
	fmt.Fprintf(os.Stderr, "  Stdout: %t\n", build.Targets.Stdout)
	fmt.Fprintf(os.Stderr, "  Images:\n")
	for _, image := range build.Targets.Images {
		fmt.Fprintf(os.Stderr, "    Name: %q\n", image.Name)
		fmt.Fprintf(os.Stderr, "    Push: %t\n", image.Push)
	}
	fmt.Fprintf(os.Stderr, "  Contexts:\n")
	for _, path := range build.Targets.Contexts {
		fmt.Fprintf(os.Stderr, "    %q\n", path)
	}
	fmt.Fprintf(os.Stderr, "registry: %q\n", build.registry)
	fmt.Fprintf(os.Stderr, "config: %q\n", build.config)
	fmt.Fprintf(os.Stderr, "parameters:\n")
	for key, value := range build.parameters {
		fmt.Fprintf(os.Stderr, "  %q →\n", key)
		fmt.Fprintf(os.Stderr, "    Name: %q\n", value.Name)
		fmt.Fprintf(os.Stderr, "    Query: %q\n", value.Query)
		fmt.Fprintf(os.Stderr, "    Value: %q\n", value.Value)
		fmt.Fprintf(os.Stderr, "    Set: %t\n", value.Set)
	}
	fmt.Fprintf(os.Stderr, "version: %q\n", build.version)
	fmt.Fprintf(os.Stderr, "commit: %q\n", build.commit)
	fmt.Fprintf(os.Stderr, "init: %t\n", build.init)
	return
}
