package relict

import (
	"archive/tar"
	"fmt"
	"io"
	"os"
	"strings"
	"syscall"
)

/*
A 'TarWriter' instance is a writer that writes tar data to its underlying
writers.

*/
type TarWriter struct {
	*tar.Writer
}

/*
'Open' tries to open and initialise 'tarWriter' with underlying writers
'writers', and returns a function 'close' that tries to close and uninitialise
'tarWriter' (but does *not* close its underlying writers).  'Open' fails if
'tarWriter' is already initialised.  'close' accepts a pointer to an error.  If
the error is 'nil' then 'close' updates it with its own error status, otherwise
'close' leaves the error unchanged.

*/
func (tarWriter *TarWriter) Open(writers ...io.Writer) (close func(*error), err error) {
	err = Initialised(tarWriter, false)
	if err != nil {
		return
	}
	tarWriter.Writer = tar.NewWriter(io.MultiWriter(writers...))
	close = func(err *error) {
		var (
			_err error
		)

		_err = tarWriter.Writer.Close()
		if *err == nil {
			*err = _err
		}
		tarWriter.Writer = nil
		return
	}
	return
}

/*
'Initialised' returns 'true' iff 'tarWriter' has been initialised by calling
its 'Open' method.

*/
func (tarWriter *TarWriter) Initialised() bool {
	return tarWriter.Writer != nil
}

/*
'AddTar' tries to add the members of the tar archive that 'tarReader' is
reading from to the tar archive that 'tarWriter' is writing to.  'AddTar' fails
if 'tarWriter' is not already initialised.

*/
func (tarWriter *TarWriter) AddTar(tarReader *tar.Reader) (err error) {
	var (
		header *tar.Header
	)

	err = Initialised(tarWriter, true)
	if err != nil {
		return
	}
	for {
		header, err = tarReader.Next()
		if err == io.EOF {
			err = nil
			return
		}
		if err != nil {
			return
		}
		err = tarWriter.WriteHeader(header)
		if err != nil {
			return
		}
		_, err = io.Copy(tarWriter, tarReader)
		if err != nil {
			return
		}
	}
	return
}

/*
'AddDir' tries to add the contents of the directory at 'path' to the tar
archive that 'tarWriter' is writing to.  'AddDir' fails if 'tarWriter' is not
already initialised.

*/
func (tarWriter *TarWriter) AddDir(path Path) (err error) {
	var (
		cleanPath   Path
		cleanInfo   os.FileInfo
		fileHandler func(string, os.FileInfo, error) error
	)

	err = Initialised(tarWriter, true)
	if err != nil {
		return
	}

	//	I clean and try to stat 'path', and ensure it is a directory.

	cleanPath = path.Clean()
	cleanInfo, err = cleanPath.Stat()
	if err != nil {
		return
	}
	if !cleanInfo.IsDir() {
		err = fmt.Errorf("'%s' is not a directory", path)
		return
	}

	//	'fileHandler' is called below by a file-system walker.  'fileHandler'
	//	examines the file with name 'fileName' and info 'fileInfo' and, based
	//	upon the properties of the file, ignores it or tries to write it to
	//	'tarWriter'.

	fileHandler = func(fileName string, fileInfo os.FileInfo, walkErr error) (err error) {
		var (
			fileMode   os.FileMode
			fileTarget string
			header     *tar.Header
		)

		//	If the walker has encountered an error reaching this file then I
		//	return that error, which stops the walker.

		err = walkErr
		if err != nil {
			return
		}

		//	I extract the mode of the file.

		fileMode = fileInfo.Mode()

		//	If the file is a socket then I return, because a socket is created
		//	by a running process and should not be written to 'tarWriter'.

		if fileMode&os.ModeSocket != 0 {
			return
		}

		//	If the file is a symbolic link then I record the link target.

		if fileMode&os.ModeSymlink != 0 {
			fileTarget, err = os.Readlink(fileName)
			if err != nil {
				return
			}
		}

		//	I try to create a prelimary tar header for the file.  If the file
		//	is a symbolic link then I set the link target I recorded above.  If
		//	the file is not a symbolic link then I set an empty link target
		//	which is ignored.

		header, err = tar.FileInfoHeader(fileInfo, fileTarget)
		if err != nil {
			return
		}

		//	I strip the cleaned 'path' (including any trailing '/') from the
		//	name entry in the tar header.  If the stripped name entry is empty
		//	then I return, because the entry is the cleaned 'path' itself and
		//	should not be written to 'tar Writer'.

		cleanPath.SetHeaderName(header, fileName)
		header.Name = strings.TrimPrefix(header.Name, `/`)
		if len(header.Name) == 0 {
			return
		}

		//	If the file is a device file then I try to set the major and minor
		//	number entries in the tar header.

		if fileMode&os.ModeDevice != 0 {
			var (
				fileStat syscall.Stat_t
			)

			err = syscall.Stat(fileName, &fileStat)
			if err != nil {
				return
			}
			header.Devmajor = int64(fileStat.Rdev / 256)
			header.Devminor = int64(fileStat.Rdev % 256)
		}

		//	I try to write the tar header to 'tarWriter'.

		err = tarWriter.WriteHeader(header)
		if err != nil {
			return
		}

		//	If the file is a plain file then I try to write the content of the
		//	file to 'tarWriter'.

		if fileMode&os.ModeType == 0 {
			var (
				fileReader *os.File
			)

			fileReader, err = os.Open(fileName)
			if err != nil {
				return
			}
			defer func(err *error) {
				var (
					_err error
				)

				_err = fileReader.Close()
				if *err == nil {
					*err = _err
				}
				return
			}(&err)
			_, err = io.Copy(tarWriter, fileReader)
			if err != nil {
				return
			}
		}
		return
	}

	//	I walk the file-system tree rooted at the clean path and pass each file
	//	I find to 'fileHandler'.  'fileHandler' examines the file and, based
	//	upon the properties of the file, ignores it or tries to write it to
	//	'tarWriter'.

	err = cleanPath.Walk(fileHandler)
	if err != nil {
		return
	}
	return
}

/*
'WriteFile' tries to write a file with name 'name', mode 'mode', user ID 'uid,
group ID 'gid' and content 'content' to the tar archive that 'tarWriter' is
writing to.  'WriteFile' fails if 'tarWriter' is not already initialised.

*/
func (tarWriter *TarWriter) WriteFile(name string, mode int64, uid int, gid int, content []byte) (err error) {
	var (
		header *tar.Header
	)

	err = Initialised(tarWriter, true)
	if err != nil {
		return
	}
	header = &tar.Header{
		Name: name,
		Mode: mode,
		Uid:  uid,
		Gid:  gid,
		Size: int64(len(content)),
	}
	err = tarWriter.WriteHeader(header)
	if err != nil {
		return
	}
	_, err = tarWriter.Write([]byte(content))
	if err != nil {
		return
	}
	return
}
