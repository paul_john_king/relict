package relict

import (
	. "path"

	"archive/tar"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/paul_john_king/revet"
)

/*
A 'Path' instance represents a file-system path.  It implements the 'flag.Value'
interface.

*/
type Path string

/*
'Abs' uses 'filepath.Abs' to try to return an absolute representation of
'path'.

*/
func (path *Path) Abs() (abs Path, err error) {
	var (
		str string
	)

	str, err = filepath.Abs(string(*path))
	if err != nil {
		return
	}
	abs = Path(str)
	return
}

/*
'Prepend' uses 'filepath.Join' to prepend 'prefix' to 'path'.

*/
func (path *Path) Prepend(prefix Path) {
	*path = Path(filepath.Join(string(prefix), string(*path)))
	return
}

/*
'Clean' uses 'path.Clean' to return 'cleanPath', the shortest 'Path' instance
equivalent to 'path' determined by purely lexical processing.

*/
func (path *Path) Clean() (cleanPath Path) {
	cleanPath = Path(Clean(string(*path)))
	return
}

/*
'Create' uses 'os.Create' to try to return a read-write file descriptor 'file'
to an empty file with path 'path'.  If the file does not exist then 'Create'
tries to create it with mode 0666 (subject to masking), otherwise 'Create'
merely tries to truncate the existing file.

TODO: Document close.

*/
func (path *Path) Create() (file *os.File, close func(*error), err error) {
	file, err = os.Create(string(*path))
	if err != nil {
		return
	}
	close = func(err *error) {
		var (
			_err error
		)

		_err = file.Close()
		if *err == nil {
			*err = _err
		}
		return
	}
	return
}

/*
'Dir' uses 'filepath.Dir' to return all but the last element of 'path'.

*/
func (path *Path) Dir() (dir Path) {
	dir = Path(filepath.Dir(string(*path)))
	return
}

/*
'IsAbs' uses 'filepath.IsAbs' to return 'true' iff 'path' is absolute.

*/
func (path *Path) IsAbs() bool {
	return filepath.IsAbs(string(*path))
}

/*
'IsEmpty' to return 'true' iff 'path' is empty.

*/
func (path *Path) IsEmpty() (empty bool) {
	return strings.Compare(string(*path), ``) == 0
}

/*
'Open' uses 'os.Open' to try to return a read-only file descriptor 'file' to an
existing file with path 'path'.

TODO: Document close

*/
func (path *Path) Open() (file *os.File, close func(*error), err error) {
	file, err = os.Open(string(*path))
	if err != nil {
		return
	}
	close = func(err *error) {
		var (
			_err error
		)

		_err = file.Close()
		if *err == nil {
			*err = _err
		}
		return
	}
	return
}

/*
'ReadFile' uses 'io/ioutil.ReadFile' to try to return the contents of an
existing file with path 'path' as byte array 'bytes'.

*/
func (path *Path) ReadFile() (bytes []byte, err error) {
	bytes, err = ioutil.ReadFile(string(*path))
	return
}

/*
'Stat' uses 'os.Stat' to try to return file-system information 'info' about an
existing file with path 'path'.

*/
func (path *Path) Stat() (info os.FileInfo, err error) {
	info, err = os.Stat(string(*path))
	return
}

/*
'Walk' uses 'path/filepath.Walk' to try to walk the file-system directory tree
rooted at 'path', calling 'function' for each file or directory in the tree,
including the one at 'path'.

*/
func (path *Path) Walk(function filepath.WalkFunc) (err error) {
	err = filepath.Walk(string(*path), function)
	return
}

/*
'SetHeaderName' sets 'header.Name' to 'name' with prefix 'path' removed if
'path' is a prefix of 'name', or just 'name' otherwise.

*/
func (path *Path) SetHeaderName(header *tar.Header, name string) {
	header.Name = strings.TrimPrefix(name, string(*path))
	return
}

/*
TODO: Document this method.

*/
func (path *Path) VersionAndCommit() (version string, commit string, err error) {
	var (
		dirty bool
	)

	version, commit, dirty, err = revet.SemanticVersion(string(*path))
	if err != nil {
		return
	}
	if dirty {
		version = version + `-dirty`
		commit = commit + `-dirty`
	}
	return
}

/*
A 'Paths' instance represents a slice of 'Path' instances.  It implements the
'flag.Value' interface.

*/
type Paths []Path
