package relict

import (
	"fmt"
	"runtime"
	"strings"
)

/*
An instance of an 'Initialisable' type has an 'Initialised' method that returns
'true' iff the instance is initialised, whatever that might mean for an
instance of the type.

*/
type Initialisable interface {
	Initialised() bool
}

/*
'Initialised' asserts whether 'object' should or should not be initialised.  It
returns an error if 'shouldBe' is 'true' but 'object' is not initialised or
'shouldBe' is 'false' but 'object' is initialised.  The error message tries to
include the name of the method that called 'Initialised'.  Any method that
requires 'object' be initialised or not should call this function with the
appropriate 'shouldBe' and check its return.

*/
func Initialised(object Initialisable, shouldBe bool) (err error) {
	if object.Initialised() != shouldBe {
		var (
			pc     uintptr
			ok     bool
			method string
		)

		pc, _, _, ok = runtime.Caller(1)
		if ok {
			method = runtime.FuncForPC(pc).Name()
			method = method[strings.LastIndex(method, `.`)+1:]
		} else {
			method = `?`
		}
		if shouldBe {
			err = fmt.Errorf("calling '%s' on an uninitialised '%T' instance is forbidden", method, object)
		} else {
			err = fmt.Errorf("calling '%s' on an initialised '%T' instance is forbidden", method, object)
		}
		return
	}
	return
}
